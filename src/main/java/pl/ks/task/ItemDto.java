package pl.ks.task;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

/**
 * Created by ks on 10.09.2019
 */

class ItemDto {

    private Integer id;

    @NotEmpty
    private String name;

    private LocalDateTime createdAt;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
