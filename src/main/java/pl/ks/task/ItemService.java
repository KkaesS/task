package pl.ks.task;

import java.util.List;

/**
 * Created by ks on 10.09.2019
 */
interface ItemService {

    ItemDto save(ItemDto item);

    List<ItemDto> findAll();

    void delete(Integer id);

}
