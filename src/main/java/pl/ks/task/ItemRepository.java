package pl.ks.task;

import java.util.List;

/**
 * Created by ks on 10.09.2019
 */
interface ItemRepository {

    Item save(Item item);

    List<Item> findAll();

    void delete(Integer id);

    boolean isExist(Integer id);

}
