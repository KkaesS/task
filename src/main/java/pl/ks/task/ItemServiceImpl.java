package pl.ks.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.ks.task.error.ItemNotFoundException;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ks on 10.09.2019
 */

@Service
class ItemServiceImpl implements ItemService {

    private final ItemRepository itemRepository;

    @Autowired
    public ItemServiceImpl(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @Transactional
    @Override
    public ItemDto save(ItemDto item) {
        Item toSave = ItemConverter.mapToItem(item);
        return ItemConverter.mapToItemDto(itemRepository.save(toSave));
    }

    @Transactional
    @Override
    public List<ItemDto> findAll() {
        List<ItemDto> itemDtoList = itemRepository.findAll()
                .stream()
                .map(ItemConverter::mapToItemDto)
                .collect(Collectors.toList());

        Collections.shuffle(itemDtoList);

        return itemDtoList;
    }

    @Transactional
    @Override
    public void delete(Integer id) {
        if (!itemRepository.isExist(id)){
            throw new ItemNotFoundException("Item with id: " + id + " not found");
        }

        itemRepository.delete(id);
    }
}
