package pl.ks.task;

/**
 * Created by ks on 10.09.2019
 */
class ItemConverter {

    private ItemConverter() {
        throw new AssertionError();
    }

    static Item mapToItem(ItemDto itemDto) {
        Item convertedItem = new Item();
        convertedItem.setId(itemDto.getId());
        convertedItem.setName(itemDto.getName());
        convertedItem.setCreatedAt(itemDto.getCreatedAt());

        return convertedItem;
    }

    static ItemDto mapToItemDto(Item item) {
        ItemDto convertedItemDto = new ItemDto();
        convertedItemDto.setId(item.getId());
        convertedItemDto.setName(item.getName());
        convertedItemDto.setCreatedAt(item.getCreatedAt());

        return convertedItemDto;
    }

}
