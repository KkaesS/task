package pl.ks.task.error;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;
import java.util.List;

/**
 * Created by ks on 10.09.2019
 */

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        pl.ks.task.error.Error error = new pl.ks.task.error.Error();
        error.setTimeStamp(new Date().getTime());
        error.setStatus(HttpStatus.BAD_REQUEST.value());

        error.setTitle("Validation Field");
        error.setDetail("Input validation field");
        error.setDeveloperMessage(ex.getClass().getName());

        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();

        for (FieldError fieldError : fieldErrors) {

            pl.ks.task.error.Error.ValidationError validationError = new pl.ks.task.error.Error.ValidationError();
            validationError.setCode(fieldError.getCode());
            validationError.setMessage(fieldError.getDefaultMessage());

            error.getErrors().putIfAbsent(fieldError.getField(), validationError);
        }

        return handleExceptionInternal(ex, error, headers, status, request);
    }


    @ExceptionHandler(ItemNotFoundException.class)
    public ResponseEntity<pl.ks.task.error.Error> handleResourceNotFoundException(ItemNotFoundException resourceNotFoundExc) {

        pl.ks.task.error.Error error = new pl.ks.task.error.Error();
        error.setTimeStamp(new Date().getTime());
        error.setStatus(HttpStatus.NOT_FOUND.value());
        error.setTitle("Item Not Found");
        error.setDetail(resourceNotFoundExc.getMessage());
        error.setDeveloperMessage(resourceNotFoundExc.getClass().getName());

        return new ResponseEntity<>(error, null, HttpStatus.NOT_FOUND);
    }
}

