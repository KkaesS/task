package pl.ks.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

/**
 * Created by ks on 10.09.2019
 */

@RestController
class ItemApi {

    private final ItemService itemService;

    @Autowired
    public ItemApi(ItemService itemService) {
        this.itemService = itemService;
    }

    @PostMapping(value = "/item", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ItemDto> saveItem(@RequestBody @Valid ItemDto itemDto) {
        ItemDto saved = itemService.save(itemDto);

        HttpHeaders httpHeaders = new HttpHeaders();
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(saved.getId())
                .toUri();
        httpHeaders.setLocation(uri);

        return new ResponseEntity<>(saved, httpHeaders, HttpStatus.OK);
    }

    @GetMapping(value = "/item", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<ItemDto> findAllItems() {
        return itemService.findAll();
    }

    @DeleteMapping("/item/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteItem(@PathVariable Integer id) {
        itemService.delete(id);
    }
}
