package pl.ks.task;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by ks on 10.09.2019
 */
@Repository
class ItemRepositoryImpl implements ItemRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Item save(Item item) {
        return entityManager.merge(item);
    }

    @Override
    public List<Item> findAll() {
        return entityManager.createQuery("select i from Item i").getResultList();
    }

    @Override
    public void delete(Integer id) {
        Item item = entityManager.find(Item.class, id);
        entityManager.remove(item);
    }

    @Override
    public boolean isExist(Integer id) {
        return entityManager.find(Item.class, id) != null;
    }
}
