package pl.ks.task;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.ks.task.error.ItemNotFoundException;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * Created by ks on 10.09.2019
 */
@ExtendWith(MockitoExtension.class)
class ItemServiceImplTest {

    @Mock
    private ItemRepository itemRepository;

    @InjectMocks
    private ItemServiceImpl itemService;

    private Item item;

    @BeforeEach
    void setUp() {
        item = new Item();
        item.setId(1);
        item.setName("item");
    }

    @Test
    void shouldSaveItem() {
        when(itemRepository.save(any(Item.class))).thenReturn(item);
        ItemDto saved = itemService.save(ItemConverter.mapToItemDto(item));

        assertEquals(item.getName(), saved.getName());
        assertEquals(1, saved.getId().intValue());

        verify(itemRepository, times(1)).save(any(Item.class));
    }

    @Test
    void shouldFindAllItems() {
        Item some = new Item();
        some.setId(2);
        some.setName("some");
        List<Item> itemList = Arrays.asList(item, some);

        when(itemRepository.findAll()).thenReturn(itemList);

        List<ItemDto> dtoList = itemService.findAll();
        assertEquals(2, dtoList.size());

        verify(itemRepository, times(1)).findAll();
    }

    @Test
    void shouldDeleteItem() {
        when(itemRepository.isExist(anyInt())).thenReturn(true);
        itemService.delete(anyInt());
        verify(itemRepository, times(1)).delete(anyInt());
    }

    @Test
    void shouldNotDeleteItem() {
        when(itemRepository.isExist(anyInt())).thenReturn(false);
        assertThrows(ItemNotFoundException.class, () -> itemService.delete(anyInt()));
        verify(itemRepository, times(0)).delete(anyInt());
    }
}