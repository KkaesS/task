package pl.ks.task;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by ks on 10.09.2019
 */

@ExtendWith(MockitoExtension.class)
class ItemApiTest {
    private static final String API_URL = "/item/";

    @Mock
    private ItemService itemService;

    @InjectMocks
    private ItemApi api;

    private MockMvc mockMvc;

    private ItemDto item;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(api).build();

        item = new ItemDto();
        item.setId(1);
        item.setName("item");
    }

    @Test
    void shouldSaveItem() throws Exception {
        when(itemService.save(any(ItemDto.class))).thenReturn(item);

        mockMvc.perform(post(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(item)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("item")));
    }


    @Test
    void shouldNotSaveItem() throws Exception {
        item.setName("");
        mockMvc.perform(post(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(item)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void shouldFindAllItems() throws Exception {
        ItemDto some = new ItemDto();
        some.setId(2);
        some.setName("another");

        List<ItemDto> itemList = Arrays.asList(item, some);
        when(itemService.findAll()).thenReturn(itemList);

        mockMvc.perform(get(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(itemList)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    void shouldDeleteItem() throws Exception {
        mockMvc.perform(delete(API_URL + "1")
                .content(new ObjectMapper().writeValueAsString(item)))
                .andExpect(status().isNoContent());
    }
}